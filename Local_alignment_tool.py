import getopt, sys
from Bio import pairwise2
optlist, args = getopt.getopt(sys.argv[1:],"k:")
file1 = open("./"+args[0],"r")
local_alignment = pairwise2.align.localxx(file1.read(),args[1])
print("Identity score :",local_alignment[0][2]," --> Error rate : %.2f" %((int(optlist[0][1])-local_alignment[0][2])/int(optlist[0][1])*100),"%")