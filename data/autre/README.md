Trois gaps sont proposés dans ce répertoire. 

La difficulté des gaps à détecter est croissante

* `gap_26997`: un gap "simple" de taille 1000 bp
* `gap_27196`: un gap avec des répétitions. Gap de taille 13000 bp
* `gap_cmpx`: un gap avec beaucoup de répétitions. Gap de taille 13000 bp

