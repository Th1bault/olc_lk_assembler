## Données projet Ecoli

Il y a dans ces exemples 2 gaps à reconstruire.

* Un gap de taille 2kb délimité par les kmers ``start_stop_2kb.fa``
* Un gap de taille 10kb délimité par les kmers ``start_stop_10kb.fa``

Les séquences à reconstruire sont dans les fichiers ``ecoli_2kb.fa`` et ``ecoli_11kb.fa`` (la séquence à reconstruire est inclue dans cette séquence)



* répertoire `gapped_reads`: les reads fournis sont limités aux reads du gap à reconstruire. La couverture est de 30x.
  * Un gap de 2kb 
    * Reads forwards parfaits
    * Reads parfaits (forward & reverse)
    * Reads 0.1% d'erreurs
    * Reads 1% d'erreurs
  * Un gap de 10kb
    * Reads parfaits (forward & reverse)
    * Reads 0.1% d'erreurs
    * Reads 1% d'erreurs
* répertoire `reads_100kb`:  les reads fournis sont  ceux des premiers 100kb de E. coli. La couverture est de 30x.
  * Reads parfaits (forward & reverse)
  * Reads 0.1% d'erreurs
  * Reads 1% d'erreurs
* répertoire `reads_1000kb`:  les reads fournis sont  ceux des premiers 1000kb de E. coli. La couverture est de 30x.
  * Reads parfaits (forward & reverse)
  * Reads 0.1% d'erreurs
  * Reads 1% d'erreurs
* répertoire `all_reads`: les reads fournis sont tous ceux du génome de E. coli. La couverture est de 30x.
  * Reads parfaits (forward & reverse)
  * Reads 0.1% d'erreurs
  * Reads 1% d'erreurs