#!/bin/bash
echo -e '\033[92;1m\n***\033[0m\033[95;1m2kb\033[0m\033[92;1m_Gapped_reads***\n\033[0m'
./Folder_test_automation.sh data/ecoli/gapped_reads data/ecoli/start_stop_2kb.fa 20 0.05
echo -e '\033[92;1m\n***\033[0m\033[95;1m10kb\033[0m\033[92;1m_Gapped_reads***\n\033[0m'
./Folder_test_automation.sh data/ecoli/gapped_reads data/ecoli/start_stop_10kb.fa 20 0.05

echo -e '\033[92;1m\n***\033[0m\033[95;1m2kb\033[0m\033[92;1m_reads_100kb***\n\033[0m'
./Folder_test_automation.sh data/ecoli/reads_100kb data/ecoli/start_stop_2kb.fa 20 0.05
echo -e '\033[92;1m\n***\033[0m\033[95;1m10kb\033[0m\033[92;1m_reads_100kb***\n\033[0m'
./Folder_test_automation.sh data/ecoli/reads_100kb data/ecoli/start_stop_10kb.fa 20 0.05

echo -e '\033[92;1m\n***\033[0m\033[95;1m2kb\033[0m\033[92;1m_reads_1000kb***\n\033[0m'
./Folder_test_automation.sh data/ecoli/reads_1000kb data/ecoli/start_stop_2kb.fa 20 0.05
echo -e '\033[92;1m\n***\033[0m\033[95;1m10kb\033[0m\033[92;1m_reads_1000kb***\n\033[0m'
./Folder_test_automation.sh data/ecoli/reads_1000kb data/ecoli/start_stop_10kb.fa 20 0.05

echo -e '\033[92;1m\n***\033[0m\033[95;1m2kb\033[0m\033[92;1m_reads_5000kb***\n\033[0m'
./Folder_test_automation.sh data/ecoli/reads_5000kb data/ecoli/start_stop_2kb.fa 20 0.05
echo -e '\033[92;1m\n***\033[0m\033[95;1m10kb\033[0m\033[92;1m_reads_5000kb***\n\033[0m'
./Folder_test_automation.sh data/ecoli/reads_5000kb data/ecoli/start_stop_10kb.fa 20 0.05